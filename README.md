# HiDesk - 小企业专用，快速远程控制电脑桌面

HiDesk是 IsoFace 提供的免费软件，安装在 Windows 操作系统的电脑，就能通过网络，使用某台电脑去遥控远程另一台电脑的操作。例如利用远程遥控即可从办公室的电脑连接至远程客户的电脑，检查用户电脑的设置与环境，以较快的速度解决各种电脑使用上的问题。

![](images/20230403152841.png)

>**请加入 QQ 群(群号:309174897)**
> 
> <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=haIxTvkUQkjCE6pNy0dH6YTuuEFYSK5_&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="物联网、智联网、ERP、" title="物联网、智联网、ERP、"></a>
> 
> **请关注微信公众号 IsoFace 爱招飞**
> 
> ![wechat_isoface.png](https://s2.loli.net/2022/10/09/WIVGP5C1E4jqbJY.png)

HiDesk 需要事先部署到电脑中，协助您连接远程的电脑桌面。并使用连接密码，确保安全性。HiDesk 支持多语言模式，连接的电脑可利用 Flying 进行网络打印工作。它包括用户端、控制端并可搭配内网穿透工具 [HiNAT](https://gitee.com/isoface-iot/HiNAT)，使用远程控制和内网穿透服务解决方案。它使用 frp 内网穿透，通过 VNC远程控制技术提供快速流畅的远程控制服务。所使用的内网穿透工具，通过图形化界面设置，提供高性能、高效的反向代理功能。

特色：

1. 搭配 [HiNAT](https://gitee.com/isoface-iot/HiNAT) 内网穿透服务程序，用户可自行部署运行测试，无需公有云服务。
2. 运用 [HiNAT](https://gitee.com/isoface-iot/HiNAT) 的 FRP 内网穿透与 VNC 远程控制，使用成熟的解决方案，远程控制带宽消耗低，网络延迟低，具有更好的远程控制效果。
3. 提供图形化界面的内网穿透配置方式，避免设置档编写导致的内网穿透服务运行错误，直观简单。
4. 提供 Web 控制介面，设置串口转发、内网穿透、反向代理服务状态，并进行监控与管理。
5. 将手机变成远程桌面，从任何地方即时访问 Windows 电脑。远程查看电脑桌面，就象坐在电脑前面一样，控制鼠标和键盘。

以往管理电脑都是到电脑前操作，但是当企业的业务型态日复杂，维护人员管理电脑的难度就越高，这时就希望能有统一的中央遥控工具，帮助管理者能在同一地方，操作所有电脑。HiDesk 是程序精实的远程遥控程序，协助小企业以最低的维护成本，进行快速遥控服务。

HiDesk 使用 [HiNAT](https://gitee.com/isoface-iot/HiNAT) 的 frp 内网穿透方案，进行远程遥控服务，HiDesk 安装简单、使用便捷。它是高性能的反向代理应用，支持多种通信协议，可以远程连接局域网设备，例如进行远程桌面、SSH等应用场景。自行搭建 [HiNAT](https://gitee.com/isoface-iot/HiNAT) 的 frp 内网穿透，使用量或速度限制取决于电脑速度和网络流量，只要电脑网络流量够多并且速度快，就可以完全发挥功能。

![](images/20230403152915.png)

运用场景：

1. 客户即时远程协助技术支持。
2. 内网部署服务提供公网访问。
3. 部署服务，实现端口转发。
4. 网站服务的反向代理，支持 SSL 协定流量的转发。
5. 物联网设备的远程存取

* **HiDesk简介**：https://isoface.net/isoface/production/tool/hidesk
* **HiDesk手册**：https://isoface.net/isoface/doc/hidesk/main/